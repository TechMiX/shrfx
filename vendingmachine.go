package main

import (
	"fmt"
	"strconv"
)

type VendingMachine struct {
	Money     int
	Inventory Storage
}

func (v *VendingMachine) Init() {
	v.Inventory.Init()
	v.Money = 0
}

func (v *VendingMachine) Insert(args ...string) {
	if len(args) < 1 {
		return
	}

	insertedMoney, _ := strconv.ParseInt(args[0], 0, 64)
	v.Money += int(insertedMoney)

	fmt.Println("Adding ", insertedMoney, " to credit")
}

func (v *VendingMachine) Order(args ...string) {
	if len(args) < 1 {
		return
	}

	sodaName := args[0]
	sodaCanBox := v.Inventory.GetSodaCanBoxByName(sodaName)
	if sodaCanBox == nil {
		return
	}

	if sodaCanBox.Count < 1 {
		fmt.Println("No", sodaCanBox.Name, "left")
		return
	}

	if v.Money < sodaCanBox.Price {
		fmt.Println("Need ", (sodaCanBox.Price - v.Money), " more")
		return
	}

	fmt.Println("Giving", sodaName, "out")
	released := v.Inventory.ReleaseFromStorage(sodaName)
	if released {
		v.Money -= sodaCanBox.Price
		fmt.Println("Giving ", v.Money, " out in change")
		v.Money = 0
	}
}

func (v *VendingMachine) Recall(args ...string) {
	fmt.Println("Returning ", v.Money, " to customer")
	v.Money = 0
}

func (v *VendingMachine) SmsOrder(args ...string) {
	if len(args) < 1 {
		return
	}

	sodaName := args[0]
	released := v.Inventory.ReleaseFromStorage(sodaName)
	if released {
		fmt.Println("Giving", sodaName, "out")
	}
}
