FROM golang:alpine AS build-env
ADD . /src
RUN cd /src && go build -o soda

FROM alpine
WORKDIR /app
COPY --from=build-env /src/soda /app/
ENTRYPOINT ./soda

# docker build -t soda-go .
# docker run -i soda-go