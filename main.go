package main

import (
	"bufio"
	"fmt"
	"os"
	"reflect"
	"strings"
)

func main() {
	v := &VendingMachine{}
	v.Init()

	v.Inventory.AddToStorage(SodaCanBox{
		Name:  "coke",
		Price: 20,
		Count: 5,
	})

	v.Inventory.AddToStorage(SodaCanBox{
		Name:  "sprite",
		Price: 15,
		Count: 3,
	})

	v.Inventory.AddToStorage(SodaCanBox{
		Name:  "fanta",
		Price: 15,
		Count: 3,
	})

	for {
		fmt.Println("\n\nAvailable commands:")
		fmt.Println("insert (money) - Money put into money slot")
		fmt.Println("order (coke, sprite, fanta) - Order from machines buttons")
		fmt.Println("sms order (coke, sprite, fanta) - Order sent by sms")
		fmt.Println("recall - gives money back")
		fmt.Println("-------")
		fmt.Println("Inserted money: ", v.Money)
		fmt.Println("-------\n\n")

		reader := bufio.NewReader(os.Stdin)
		inputLine, _ := reader.ReadString('\n')
		inputLine = strings.TrimSpace(inputLine)

		inputs := strings.Split(inputLine, " ")

		vReflection := reflect.ValueOf(v)
		command := vReflection.MethodByName(strings.Title(inputs[0]))
		args := inputs[1:]

		if !command.IsValid() {
			if len(inputs) > 1 {
				command = vReflection.MethodByName(strings.Title(inputs[0]) + strings.Title(inputs[1]))
				if !command.IsValid() {
					continue
				}
				args = inputs[2:]
			} else {
				continue
			}
		}

		var argsReflection []reflect.Value
		for i := 0; i < len(args); i++ {
			argsReflection = append(argsReflection, reflect.ValueOf(args[i]))
		}

		command.Call(argsReflection)

	}

}
