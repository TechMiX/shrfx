package main

type Storage struct {
	storage            []SodaCanBox
	storageIndexByName map[string]int
}

func (i *Storage) Init() {
	i.storageIndexByName = make(map[string]int)
}

func (i *Storage) GetSodaCanBoxByName(sodaName string) *SodaCanBox {
	if index, ok := i.storageIndexByName[sodaName]; ok {
		return &i.storage[index]
	}
	return nil
}

func (i *Storage) AddToStorage(newSoda SodaCanBox) {
	if index, ok := i.storageIndexByName[newSoda.Name]; ok {
		i.storage[index].Count += newSoda.Count
	} else {
		i.storage = append(i.storage, newSoda)
		i.storageIndexByName[newSoda.Name] = len(i.storage) - 1
	}
}

func (i *Storage) ReleaseFromStorage(sodaName string) bool {
	index, ok := i.storageIndexByName[sodaName]
	if ok && i.storage[index].Count > 0 {
		i.storage[index].Count--
		return true
	}
	return false
}
